import os
import random as rd
import csv
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pandas_profiling import ProfileReport
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cluster import KMeans
from sklearn.impute import SimpleImputer
from sklearn import datasets
from sklearn.linear_model import LogisticRegression

## Part 1
input1 = "./PROJECT/Data/Part1.csv"
input3 = "./PROJECT/Data/Part3.csv"
output = "./PROJECT/Output/"

# Import the datasets
df = pd.read_csv(os.path.abspath(input1), sep=",")
df2 = pd.read_csv(os.path.abspath(input3), sep=",")

profile = ProfileReport(df, title="Pandas Profiling Report part 1")
profile.to_notebook_iframe()

# take a look at the data
df.head()
df = pd.DataFrame(data=df)
df.dtypes
df2.dtypes

###

##  Plot of the distribution of the cost in each course


# Make a separate list for each course
x1 = list(df['FIRST_COURSE'])
x2 = list(df['SECOND_COURSE'])
x3 = list(df['THIRD_COURSE'])

# Assign colors for each course and the names
colors = ['#E69F00', '#56B4E9', '#F0E442']

names = ['First Course', 'Second Course', 'Third Course']

# Make the histogram using a list of lists
# Assign colors and names
plt.hist([x1, x2, x3], bins=int(180 / 15),
         color=colors, label=names)

# Plot formatting
plt.legend()
plt.xlabel('Cost')
plt.ylabel('Number of course')
plt.title('Distribution of the cost in each course')
plt.savefig(os.path.abspath(output) + '/Distribution of the cost in each course')
plt.clf()
##  Plot of the distribution of the cost in each time
## Lunch

# Make a separate list for each course
x1 = list(df[df['TIME'] == 'LUNCH']['FIRST_COURSE'])
x2 = list(df[df['TIME'] == 'LUNCH']['SECOND_COURSE'])
x3 = list(df[df['TIME'] == 'LUNCH']['THIRD_COURSE'])

# Assign colors for each course and the names
colors = ['#E69F00', '#56B4E9', '#F0E442']

names = ['First Course', 'Second Course', 'Third Course']

# Make the histogram using a list of lists
# Assign colors and names
plt.hist([x1, x2, x3], bins=int(180 / 15),
         color=colors, label=names)

# Plot formatting
plt.legend()
plt.xlabel('Price')
plt.ylabel('Frequence')
plt.title('Distribution of the course price during the lunch')
plt.savefig(os.path.abspath(output) + '/Distribution of the course price during the lunch')
plt.clf()

##  plot of the distribution of the cost in each time
## Dinner

# Make a separate list for each course
x1 = list(df[df['TIME'] == 'DINNER']['FIRST_COURSE'])
x2 = list(df[df['TIME'] == 'DINNER']['SECOND_COURSE'])
x3 = list(df[df['TIME'] == 'DINNER']['THIRD_COURSE'])

# Assign colors for each course and the names
colors = ['#E69F00', '#56B4E9', '#F0E442']

names = ['First Course', 'Second Course', 'Third Course']

# Make the histogram using a list of lists
# Assign colors and names
plt.hist([x1, x2, x3], bins=int(180 / 15),
         color=colors, label=names)

# Plot formatting
plt.legend()
plt.xlabel('Price')
plt.ylabel('Frequence')
plt.title('Distribution of the course price during the dinner')
plt.savefig(os.path.abspath(output) + '/Distribution of the course price during the dinner')
plt.clf()

## Function to capture the price of drink per course
##########
menu_1_price = [3, 15, 20]
menu_2_price = [9, 20, 25, 40]
menu_3_price = [10, 15]


def price_drink(df, menu):
    drinkPriceList = []
    for i in df.values:
        drink_price = 10000000  # high value to be able to start the loop the right way
        for j in menu:
            if (i >= j):
                drink_price = min(abs(i - j), drink_price);
            else:
                if (drink_price == 10000000):
                    drink_price = 0
                break;
        if drink_price not in menu:
            drinkPriceList.append(drink_price)
        else:
            drinkPriceList.append(0.0)
    return drinkPriceList


##

menuFood1 = ["Soup", "Tomato-Mozarella", "Oysters"]
menuFood2 = ["Salad", "Spaghetti", "Steak", "Lobster"]
menuFood3 = ["Pie", "Ice cream"]


def whichMeal(dfCourse, dfDrink, menuFood, menuDrink):
    FoodList = []
    for i in (dfCourse.values - dfDrink.values):
        # print(i)
        if i == 0.0:
            FoodList.append("NO FOOD")
        else:
            for j in range(len(menuDrink)):
                if i == menuDrink[j]:
                    FoodList.append(menuFood[j])
    return FoodList


## Add variables that unstack different elements to the data frame
df["first_drink"] = price_drink(df["FIRST_COURSE"], menu_1_price)
df["second_drink"] = price_drink(df["SECOND_COURSE"], menu_2_price)
df["third_drink"] = price_drink(df["THIRD_COURSE"], menu_3_price)
df["first_meal"] = whichMeal(df["FIRST_COURSE"], df["first_drink"], menuFood1, menu_1_price)
df["second_meal"] = whichMeal(df["SECOND_COURSE"], df["second_drink"], menuFood2, menu_2_price)
df["third_meal"] = whichMeal(df["THIRD_COURSE"], df["third_drink"], menuFood3, menu_3_price)

##  clustering using kmeans methodology
dftemp = df[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]]

X = np.array(dftemp)

n_initList = [5, 10, 100]

estimators = [("4 clusters with n_init = 5", KMeans(n_clusters=4, n_init=100))
              # ("4 clusters with n_init = 10" ,KMeans(n_clusters=4, n_init = 10)),
              # ("4 clusters with n_init = 100" ,KMeans(n_clusters=4, n_init = 100))
              ]

fignum = 1

for title, est in estimators:
    fig = plt.figure(fignum, figsize=(12, 9))
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
    est.fit(X)
    labels = est.labels_

    print(labels)
    dftemp["labels"] = labels

    ## Creating the scatter plot for the cluster
    scatter = ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=labels.astype(np.float))

    legend = ax.legend(*scatter.legend_elements(), title="labels")

    ax.add_artist(legend)

    ax.set_xlabel('FIRST COURSE')
    ax.set_ylabel('SECOND COURSE')
    ax.set_zlabel('THIRD COURSE')
    ax.set_title(title)
    ax.dist = 12
    fignum += 1

    fig.savefig(os.path.abspath(output) + '/Clustering of the customers', bbox_inches='tight')
    plt.clf()

## Attribute label(client type) from the clustering to each client in the data

df["labels"] = dftemp['labels']

Labels = []
for i in df["labels"].values:
    if i == 2:
        Labels.append("Retirement")
    elif i == 1:
        Labels.append("Business")
    elif i == 3:
        Labels.append("Healthy")
    elif i == 0:
        Labels.append("Onetime")

df["CLIENT_TYPE"] = Labels

## Create a dataframe to build a confusion matrix
df_matrix = df[["CLIENT_TYPE"]]
df_matrix["Expected"] = df2["CLIENT_TYPE"].values

## Confusion matrix to compare the simulation with actual data

df_matrix = pd.DataFrame(df_matrix, columns=['Expected', 'CLIENT_TYPE'])
confusion_matrix = pd.crosstab(df_matrix['Expected'], df_matrix['CLIENT_TYPE'], rownames=['Actual'],
                               colnames=['Predicted'])

sns.heatmap(confusion_matrix, annot=True)
plt.savefig(os.path.abspath(output) + '/Confusion matrix')
plt.clf()

##To determine the distribution of the clients
## Create a dataframe then do the statistics
df3 = df2.groupby('CLIENT_TYPE').size()
df3.plot.pie(subplots=True, figsize=(11, 6), autopct='%1.1f%%')
plt.savefig(os.path.abspath(output) + '/Pie chart of client distribution')
plt.clf()
# To determine the likelihood for each of these clients to get a certain course.
# 1st joint the initial data with the one where we know the client type
df = df.drop(columns="CLIENT_TYPE")
join_df = df.merge(df2, on='CLIENT_ID', how='left')

## Create a dataframe whrere there are the data of only one type of customer
df_business = join_df[join_df["CLIENT_TYPE"] == "Business"][
    ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE", "first_meal", "second_meal", "third_meal"]]
df_onetime = join_df[join_df["CLIENT_TYPE"] == "Onetime"][
    ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE", "first_meal", "second_meal", "third_meal"]]
df_healthy = join_df[join_df["CLIENT_TYPE"] == "Healthy"][
    ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE", "first_meal", "second_meal", "third_meal"]]
df_retirement = join_df[join_df["CLIENT_TYPE"] == "Retirement"][
    ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE", "first_meal", "second_meal", "third_meal"]]

## Divide the number of course for each customer on the total to get the likelihood
b = (df_business != 0).sum() / df_business.count()
o = (df_onetime != 0).sum() / df_onetime.count()
h = (df_healthy != 0).sum() / df_healthy.count()
r = (df_retirement != 0).sum() / df_retirement.count()
#
print("Likelihood of businessman to take different course", "\n", b[0:3] * 100, "\n")
print("Likelihood of onetime customers to take different course", "\n", o[0:3] * 100, "\n")
print("Likelihood of healthy customers to take different course", "\n", h[0:3] * 100, "\n")
print("Likelihood of retired customers to take different course", "\n", r[0:3] * 100, "\n")

## Probability of certain type of customer ordering a certain dish
## Create a cross table for each meal then we look at the data frame created to see the result
## Cross table of different dish for the starter (First meal)
first_meal = pd.crosstab(index=join_df["CLIENT_TYPE"],
                         columns=join_df["first_meal"], margins=True, margins_name='Total')

first_meal.index = ["Business", "Healthy", "Onetime", "Retirement", "Total"]
first_meal["Oysters"] = first_meal["Oysters"].div(first_meal["Total"].values)
first_meal["Soup"] = first_meal["Soup"].div(first_meal["Total"].values)
first_meal["Tomato-Mozarella"] = first_meal["Tomato-Mozarella"].div(first_meal["Total"].values)
first_meal["NO FOOD"] = first_meal["NO FOOD"].div(first_meal["Total"].values)

## It's the frequence of each dish per course
## the variable no food is not a the frequence
## The variable no food means that they did not order something for the first course


print("Probability of the customer buying a certain dish for the first course", "\n", "\n", first_meal.head())

## Cross table of different dish for the second meal (Main)
second_meal = pd.crosstab(index=join_df["CLIENT_TYPE"],
                          columns=join_df["second_meal"], margins=True, margins_name='Total')

second_meal.index = ["Business", "Healthy", "Onetime", "Retirement", "Total"]
second_meal["Lobster"] = second_meal["Lobster"].div(second_meal["Total"].values)
second_meal["Salad"] = second_meal["Salad"].div(second_meal["Total"].values)
second_meal["Spaghetti"] = second_meal["Spaghetti"].div(second_meal["Total"].values)
second_meal["Steak"] = second_meal["Steak"].div(second_meal["Total"].values)

## It's the frequence of each dish per course
print("Probability of the customer buying a certain dish for the second course", "\n", "\n", second_meal.head())

# Cross table of different dish for the third meal (Dessert)
third_meal = pd.crosstab(index=join_df["CLIENT_TYPE"],
                         columns=join_df["third_meal"], margins=True, margins_name='Total')

third_meal['Total'] = third_meal['Total'] - third_meal['NO FOOD']
third_meal.index = ["Business", "Healthy", "Onetime", "Retirement", "Total"]
third_meal["Ice cream"] = third_meal["Ice cream"].div(third_meal["Total"].values)
third_meal["Pie"] = third_meal["Pie"].div(third_meal["Total"].values)
third_meal["NO FOOD"] = third_meal["NO FOOD"].div(third_meal["Total"].values)

## It's the frequence of each dish per course
## the variable no food is not a the frequence
## The variable no food means that they did not order something for the third course
print("Probability of the customer buying a certain dish for the third course", "\n", "\n", third_meal.head())

## Distribution of dishes, per course, per customer
##For example, once you have labelled all clients
##you can just check the averages of how often a client of this type gets a starters, or main, or a dessert.
##similarly you can do it for the indidivual dishes.

print("Distribution of First course by type of client when they go to the restaurant", "\n", "\n",
      first_meal['Total'] / (first_meal['Total'] + second_meal['Total'] + third_meal['Total']), "\n")
print("Distribution of Second course by type of client when they go to the restaurant", "\n", "\n",
      second_meal['Total'] / (first_meal['Total'] + second_meal['Total'] + third_meal['Total']), "\n")
print("Distribution of Third course by type of client when they go to the restaurant", "\n", "\n",
      third_meal['Total'] / (first_meal['Total'] + second_meal['Total'] + third_meal['Total']), "\n")

## Distribution of type of dishes per customer type
# first Course

join_df.groupby(['CLIENT_TYPE', 'first_meal']).size().unstack().plot(kind='bar')
# We change the fontsize of minor ticks label
ax.tick_params(axis='both', which='major', labelsize=10)
ax.tick_params(axis='both', which='minor', labelsize=8)

plt.savefig(os.path.abspath(output) + '/Distribution of dishes for the first course per customer type')
plt.clf()

# Second Course
join_df.groupby(['CLIENT_TYPE', 'second_meal']).size().unstack().plot(kind='bar')
plt.savefig(os.path.abspath(output) + '/Distribution of dishes for the second course per customer type')
plt.clf()

# Third Course
join_df.groupby(['CLIENT_TYPE', 'third_meal']).size().unstack().plot(kind='bar')
plt.savefig(os.path.abspath(output) + '/Distribution of dishes for the third course per customer type')
plt.clf()

## Cost of the drinks per course
##  Plot of the distribution of the cost in each course
# Make a separate list for each course
x1 = list(df['first_drink'])
x2 = list(df['second_drink'])
x3 = list(df['third_drink'])

# Assign colors for each course and the names
colors = ['#E69F00', '#56B4E9', '#F0E442']

names = ['First course', 'Second course', 'Third course']

# Make the histogram using a list of lists
# Assign colors and names
plt.hist([x1, x2, x3], bins=int(180 / 15),
         color=colors, label=names)

# Plot formatting
plt.legend()
plt.xlabel('Cost')
plt.ylabel('Number of drink')
plt.title('Distribution of the cost of drink per course')
plt.savefig(os.path.abspath(output) + '/Distribution of the cost of drink per course')
plt.clf()


## Part 4
class Client(object):
    menu = [["NoFood", "Soup", "Tomato-Mozarella", "Oysters"], ["Salad", "Spaghetti", "Steak", "Lobster"],
            ["Ice cream", "Pie", "NoFood"]]
    weight = [[0.998698, 0.000206, 0.000343, 0.000754], [0, 0.096785, 0.805744, 0.097471], [0, 0.001302, 0.998698]]
    probability = 0

    def __init__(self, _ID):
        self.ID = _ID

    # this function returns in a list the list of the orders of the client
    # and if the client will return to eat to the restaurant
    def goToRestaurant(self, ):
        courses = []
        for i in range(3):
            courses.append(rd.choices(self.menu[i], weights=self.weight[i])[0])
        return [rd.choices([True, False], [self.probability, 100 - self.probability])[0]
            , courses]


class BusinessClient(Client):
    weight = [[0.303491, 0.062697, 0, 0.633812], [0.102943, 0.096099, 0.100890, 0.700068],
              [0.081588, 0.195756, 0.722656]]
    probability = 50

    def __init__(self, _ID):
        Client.__init__(self, _ID)


class HealtyClient(Client):
    weight = [[0, 1, 0, 0], [1, 0, 0, 0], [0.009131, 0, 0.990869]]
    probability = 70

    def __init__(self, ID):
        Client.__init__(self, ID)


class RetirementClient(Client):
    weight = [[0.098514, 0.276555, 0.272289, 0.352642], [0.100303, 0.600303, 0.197166, 0.102229],
              [0.098514, 0.901486, 0]]
    probability = 90

    def __init__(self, ID):
        Client.__init__(self, ID)


# this function write on the header of the csv file
# called data.csv then all of the dishes are written
# on the file
def write_csv(pathcsv):
    header = ['TIME', 'CUSTOMER_ID', 'CUSTOMER_TYPE', 'COURSE1', 'COURSE2', 'COURSE3',
              'DRINKS1', 'DRINKS2', 'DRINKS3',
              'TOTAL1', 'TOTAL2', 'TOTAL3']
    with open(pathcsv, 'w', encoding='UTF8') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header)

        for row in ListData:
            writer.writerow(row)


# This function takes in parameter a list of client
# and returned the number of each type of client
# of the list
def getNumberOfTypeOfClients(clients):
    nbForEachGroup = {
        "RetirementClient": 0,
        "HealtyClient": 0,
        "BusinessClient": 0
    }
    for client in clients:
        typeClient = type(client).__name__
        nbForEachGroup[typeClient] += 1

    return nbForEachGroup


# This function takes in parameter : an integer
# which indicates where to start the initialisation
# of the IDs of clients and takes in parameter a list
# containing the list of client who return to eat for
# the day to simulate
# This function simulate a day in the restaurant by
# storing the clients who ate for the day and returning
# the clients who will return for the next day

def simulateaDay(_ID, clientWhoReturn):
    ID = _ID
    clientsOfTheDay = clientWhoReturn
    nbReturnDict = getNumberOfTypeOfClients(clientWhoReturn)
    # Adding all clients for the day in the right proportion
    for j in range(NbDishes * 20 // 100 - nbReturnDict["BusinessClient"]):  # 20% in proportion of client each day
        clientsOfTheDay.append(BusinessClient(ID))
        ID += 1
    for j in range(NbDishes * 20 // 100 - nbReturnDict["RetirementClient"]):  # 20% in proportion of client each day
        clientsOfTheDay.append(RetirementClient(ID))
        ID += 1
    for j in range(NbDishes * 20 // 100 - nbReturnDict["HealtyClient"]):  # 20% in proportion of client each day
        clientsOfTheDay.append(HealtyClient(ID))
        ID += 1
    while len(clientsOfTheDay) < 20:  # 40% in proportion of client each day
        clientsOfTheDay.append(Client(ID))
        ID += 1

    clientWhoWillReturn = []
    # Getting the orders of evey client
    for client in clientsOfTheDay:
        # Add to the csv
        # Lunch or dinner
        LunchOrDinner = rd.choices(["Lunch", "Dinner"], weights=[50, 50])
        # Getting the courses of clients and if they will return
        willReturn, courses = client.goToRestaurant()
        # Getting the price or the dishes
        menuprice = [menuDict[courses[0]], menuDict[courses[1]], menuDict[courses[2]]]
        # Geting the price of drinks
        drinkprice = [rd.random() * 4.999957, rd.random() * 2, rd.random() * 2]  # max price per drink below 2
        # Getting the price of each courses
        totals = [menuprice[0] + drinkprice[0], menuprice[1] + drinkprice[1], menuprice[2] + drinkprice[2]]
        # Getting the type of client
        typeClient = type(client).__name__
        # Storing the client orders the main list containing all orders
        data = [LunchOrDinner[0], client.ID, typeClient, courses[0], courses[1],
                courses[2], drinkprice[0], drinkprice[1], drinkprice[2], totals[0], totals[1], totals[2]]
        ListData.append(data)

        # Storing the client who will return to eat
        if willReturn:
            clientWhoWillReturn.append(client)

    return clientWhoWillReturn


# Parameters of the simulation
rd.seed(10)  # seed defined to get the same results for the report
nextID = 0
ListData = []
NbDishes = 20
clientsOfTheDay = []
menuDict = {
    "NoFood": 0.0,
    "Soup": 3.0,
    "Tomato-Mozarella": 15.0,
    "Oysters": 20.0,
    "Salad": 9.0,
    "Spaghetti": 20.0,
    "Steak": 25.0,
    "Lobster": 50.0,
    "Ice cream": 15.0,
    "Pie": 10.0
}

# Starting of the simulation
clientWhoReturne = []
print("Simulation started...")
for j in range(5):
    for i in range(365):
        FutureClients = simulateaDay(nextID, clientWhoReturne)
        nextID = nextID + NbDishes
        clientWhoReturne = FutureClients
    print("One year finished")
print("writing data on the csv file...")
write_csv('../Data/dataIncreasedPriceOfLobster.csv')
print("End of the simulation")

# Analyzing the revenue depending on the situation
import pandas as pd  # importated again to facilitate execution of code portion
import os

initialSimulation = pd.read_csv("../Data/dataSimulated.csv", sep=",")
twiceMoreHealthy = pd.read_csv("../Data/dataTwiceMoreHealthy.csv", sep=",")
increasedPriceOfSpaghetti = pd.read_csv("../Data/dataIncreasedPriceOfSpaghetti.csv", sep=",")
increasedPriceOfLobster = pd.read_csv("../Data/dataIncreasedPriceOfLobster.csv", sep=",")

initialSimulation["sum"] = initialSimulation["TOTAL1"] + initialSimulation["TOTAL2"] + initialSimulation["TOTAL3"]
twiceMoreHealthy["sum"] = twiceMoreHealthy["TOTAL1"] + twiceMoreHealthy["TOTAL2"] + twiceMoreHealthy["TOTAL3"]
increasedPriceOfSpaghetti["sum"] = increasedPriceOfSpaghetti["TOTAL1"] + increasedPriceOfSpaghetti["TOTAL2"] + \
                                   increasedPriceOfSpaghetti["TOTAL3"]
increasedPriceOfLobster["sum"] = increasedPriceOfLobster["TOTAL1"] + increasedPriceOfLobster["TOTAL2"] + \
                                 increasedPriceOfLobster["TOTAL3"]

print("Revenue with the classic simulation : " + "$" + str(initialSimulation["sum"].sum()))
print("Revenue with twice more healthy customers : " + "$" + str(twiceMoreHealthy["sum"].sum()))
print("Revenue with the increase price of spaghetti to $30 : " + "$" + str(increasedPriceOfSpaghetti["sum"].sum()))
print("Revenue with the increase price of lobster to $50 : " + "$" + str(increasedPriceOfLobster["sum"].sum()))
