import os
import random as rd
import csv
import pandas as pd


class Client(object):

    menu = [["NoFood","Soup", "Tomato-Mozarella", "Oysters"], ["Salad", "Spaghetti", "Steak", "Lobster"], ["Ice cream", "Pie","NoFood"]]
    weight = [[0.998698, 0.000206, 0.000343, 0.000754], [0, 0.096785, 0.805744, 0.097471], [0, 0.001302, 0.998698]]
    probability = 0

    def __init__(self, _ID):
        self.ID = _ID

    # this function returns in a list the list of the orders of the client
    # and if the client will return to eat to the restaurant
    def goToRestaurant(self,):
        courses = []
        for i in range(3):
            courses.append(rd.choices(self.menu[i], weights=self.weight[i])[0])
        return [rd.choices([True, False], [self.probability, 100-self.probability])[0]
            , courses]


class BusinessClient(Client):
    weight = [[0.303491, 0.062697, 0, 0.633812], [0.102943, 0.096099, 0.100890, 0.700068], [0.081588, 0.195756, 0.722656]]
    probability = 50

    def __init__(self, _ID):
        Client.__init__(self, _ID)


class HealtyClient(Client):
    weight = [[0, 1, 0, 0], [1, 0, 0, 0], [0.009131, 0, 0.990869]]
    probability = 70

    def __init__(self, ID):
        Client.__init__(self, ID)


class RetirementClient(Client):
    weight = [[0.098514, 0.276555, 0.272289, 0.352642], [0.100303, 0.600303, 0.197166, 0.102229], [0.098514, 0.901486, 0]]
    probability = 90

    def __init__(self, ID):
        Client.__init__(self, ID)


# this function write on the header of the csv file
# called data.csv then all of the dishes are written
# on the file
def write_csv(pathcsv):
    header = ['TIME', 'CUSTOMER_ID', 'CUSTOMER_TYPE', 'COURSE1', 'COURSE2', 'COURSE3',
              'DRINKS1', 'DRINKS2', 'DRINKS3',
              'TOTAL1', 'TOTAL2', 'TOTAL3']
    with open(pathcsv, 'w', encoding='UTF8') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(header)

        for row in ListData:
            writer.writerow(row)


# This function takes in parameter a list of client
# and returned the number of each type of client
# of the list
def getNumberOfTypeOfClients(clients):
    nbForEachGroup = {
        "RetirementClient": 0,
        "HealtyClient": 0,
        "BusinessClient": 0
    }
    for client in clients:
        typeClient = type(client).__name__
        nbForEachGroup[typeClient] += 1

    return nbForEachGroup


# This function takes in parameter : an integer
# which indicates where to start the initialisation
# of the IDs of clients and takes in parameter a list
# containing the list of client who return to eat for
# the day to simulate
# This function simulate a day in the restaurant by
# storing the clients who ate for the day and returning
# the clients who will return for the next day

def simulateaDay(_ID, clientWhoReturn):
    ID = _ID
    clientsOfTheDay = clientWhoReturn
    nbReturnDict = getNumberOfTypeOfClients(clientWhoReturn)
    # Adding all clients for the day in the right proportion
    for j in range(NbDishes * 20 // 100 - nbReturnDict["BusinessClient"]): #20% in proportion of client each day
        clientsOfTheDay.append(BusinessClient(ID))
        ID += 1
    for j in range(NbDishes * 20 // 100 - nbReturnDict["RetirementClient"]):  #20% in proportion of client each day
        clientsOfTheDay.append(RetirementClient(ID))
        ID += 1
    for j in range(NbDishes * 20 // 100 - nbReturnDict["HealtyClient"]):  #20% in proportion of client each day
        clientsOfTheDay.append(HealtyClient(ID))
        ID += 1
    while len(clientsOfTheDay) < 20:  #40% in proportion of client each day
        clientsOfTheDay.append(Client(ID))
        ID += 1

    clientWhoWillReturn = []
    # Getting the orders of evey client
    for client in clientsOfTheDay:
        # Add to the csv
        # Lunch or dinner
        LunchOrDinner = rd.choices(["Lunch", "Dinner"], weights=[50, 50])
        # Getting the courses of clients and if they will return
        willReturn, courses = client.goToRestaurant()
        # Getting the price or the dishes
        menuprice = [menuDict[courses[0]], menuDict[courses[1]], menuDict[courses[2]]]
        # Geting the price of drinks
        drinkprice = [rd.random() * 4.999957, rd.random() * 2, rd.random() * 2]  # max price per drink below 2
        # Getting the price of each courses
        totals = [menuprice[0] + drinkprice[0], menuprice[1] + drinkprice[1], menuprice[2] + drinkprice[2]]
        # Getting the type of client
        typeClient = type(client).__name__
        # Storing the client orders the main list containing all orders
        data = [LunchOrDinner[0], client.ID, typeClient, courses[0], courses[1],
                courses[2], drinkprice[0], drinkprice[1], drinkprice[2], totals[0], totals[1], totals[2]]
        ListData.append(data)

        # Storing the client who will return to eat
        if willReturn:
            clientWhoWillReturn.append(client)

    return clientWhoWillReturn


# Parameters of the simulation
rd.seed(10) # seed defined to get the same results for the report
nextID = 0
ListData = []
NbDishes = 20
clientsOfTheDay = []
menuDict = {
    "NoFood": 0.0,
    "Soup": 3.0,
    "Tomato-Mozarella": 15.0,
    "Oysters": 20.0,
    "Salad": 9.0,
    "Spaghetti": 20.0,
    "Steak": 25.0,
    "Lobster": 50.0,
    "Ice cream": 15.0,
    "Pie": 10.0
}

# Starting of the simulation
clientWhoReturne = []
print("Simulation started...")
for j in range(5):
    for i in range(365):
        FutureClients = simulateaDay(nextID, clientWhoReturne)
        nextID = nextID + NbDishes
        clientWhoReturne = FutureClients
    print("One year finished")
print("writing data on the csv file...")
write_csv('../Data/dataIncreasedPriceOfLobster.csv')
print("End of the simulation")

#Analyzing the revenue depending on the situation
import pandas as pd # importated again to facilitate execution of code portion
import os
initialSimulation = pd.read_csv("../Data/dataSimulated.csv", sep=",")
twiceMoreHealthy = pd.read_csv("../Data/dataTwiceMoreHealthy.csv", sep=",")
increasedPriceOfSpaghetti = pd.read_csv("../Data/dataIncreasedPriceOfSpaghetti.csv", sep=",")
increasedPriceOfLobster= pd.read_csv("../Data/dataIncreasedPriceOfLobster.csv", sep=",")

initialSimulation["sum"] = initialSimulation["TOTAL1"] + initialSimulation["TOTAL2"] + initialSimulation["TOTAL3"]
twiceMoreHealthy["sum"] = twiceMoreHealthy["TOTAL1"] + twiceMoreHealthy["TOTAL2"] + twiceMoreHealthy["TOTAL3"]
increasedPriceOfSpaghetti["sum"] = increasedPriceOfSpaghetti["TOTAL1"] + increasedPriceOfSpaghetti["TOTAL2"] + increasedPriceOfSpaghetti["TOTAL3"]
increasedPriceOfLobster["sum"] = increasedPriceOfLobster["TOTAL1"] + increasedPriceOfLobster["TOTAL2"] + increasedPriceOfLobster["TOTAL3"]

print("Revenue with the classic simulation : " + "$" + str(initialSimulation["sum"].sum()))
print("Revenue with twice more healthy customers : " + "$" + str(twiceMoreHealthy["sum"].sum()))
print("Revenue with the increase price of spaghetti to $30 : " + "$" + str(increasedPriceOfSpaghetti["sum"].sum()))
print("Revenue with the increase price of lobster to $50 : " + "$" + str(increasedPriceOfLobster["sum"].sum()))
