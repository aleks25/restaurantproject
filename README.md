# README #

Python project made by Aleksandar DJINDJIC and Aser Balima BOBLAWENDE

### Summary ###

You will find several notebooks for the different parts of the project and a python file for the simulation.
For the different parts the user may need to change the path to access the data depending on the configuration.

### Part1_3_Notebook
We have use jupyter notebook because it was more convenient for the data visualization.

### Part_4_simulation 
It contains only the simulation that made for the part 4.

### AnalyzingSimulatedData 
It contains only the code in order to analyze the data created by the simualtion.


### Part_1_4
It is the whole project on pycharm. 
If the user only wants to work on pycharm he can use it. 
It contains all the project from part 1 to 4. 
The user may face some issues when running the entire code at once, in this situation you can either use the Notebook 
or run the code piece by pieces through the python console. The outputs will be exactly the same. 
